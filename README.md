# Ansible Role: etckeeper

This role installs etckeeper and creates initial commit.

## Mandatory variables

As etckeeper uses git, the git users name and email address needs to be set.
This can be achieved i.e. in _inventory/production/group_vars/all.yml_:

```
# etckeeper uses git and git needs some things defined
etckeeper__vcs_user: "First LastName"
etckeeper__vcs_email: "email@some.tld"

```

## Sources

- https://github.com/debops-contrib/ansible-etckeeper
